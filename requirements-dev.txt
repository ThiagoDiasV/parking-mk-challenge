appdirs==1.4.3
asgiref==3.2.3
attrs==19.3.0
backcall==0.1.0
black==19.10b0
Click==7.0
decorator==4.4.1
Django==3.0.2
django-filter==2.2.0
djangorestframework==3.11.0
ipdb==0.12.3
ipython==7.11.1
ipython-genutils==0.2.0
jedi==0.15.2
mccabe==0.6.1
mypy==0.761
mypy-extensions==0.4.3
parso==0.5.2
pathspec==0.7.0
pexpect==4.7.0
pickleshare==0.7.5
prompt-toolkit==3.0.2
ptyprocess==0.6.0
pycodestyle==2.5.0
pydocstyle==5.0.1
pyflakes==2.1.1
Pygments==2.5.2
pylama==7.7.1
pytz==2019.3
regex==2020.1.8
six==1.13.0
snowballstemmer==2.0.0
sqlparse==0.3.0
toml==0.10.0
traitlets==4.3.3
typed-ast==1.4.0
typing-extensions==3.7.4.1
wcwidth==0.1.8
